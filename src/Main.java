import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main
{
	private long findOccurrences(String source, char character)
	{
		return source.chars().filter(ch -> ch == character).count();
	}
	
	private void Ex1()
	{
		Scanner scanner = new Scanner(System.in);
		
		String string;
		char character;
		try
		{
			System.out.print("String: ");
			string = scanner.nextLine();
			System.out.print("Character: ");
			character = scanner.next().charAt(0);
		}
		catch (InputMismatchException e)
		{
			e.printStackTrace();
			return;
		}
		
		System.out.println("Occurrences of " + character + ": " + findOccurrences(string, character));
	}
	
	@SuppressWarnings("SpellCheckingInspection")
	private void Ex2()
	{
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("String: ");
		String string = scanner.nextLine();
		
		Pattern exponentialPattern = Pattern.compile("-*[0-9]+\\.[0-9]+E-*[0-9]+");
		Pattern realPattern = Pattern.compile("-*[0-9]+\\.[0-9]+");
		Pattern integerPattern = Pattern.compile("-[0-9]+");
		Pattern naturalPattern = Pattern.compile("[0-9]+");
		
		
		Matcher exponentialMatcher = exponentialPattern.matcher(string);
		Matcher realMatcher = realPattern.matcher(string);
		Matcher integerMatcher = integerPattern.matcher(string);
		Matcher naturalMatcher = naturalPattern.matcher(string);
		
		if (exponentialMatcher.matches())
		{
			System.out.println("Sirul dat reprezinta un numar real in forma exponentiala.");
		}
		else if (realMatcher.matches())
		{
			System.out.println("Sirul dat reprezinta un numar real.");
		}
		else if (integerMatcher.matches())
		{
			System.out.println("Sirul dat reprezinta un numar intreg.");
		}
		else if (naturalMatcher.matches())
		{
			System.out.println("Sirul dat reprezinta un numar natural.");
		}
		else
		{
			System.out.println("Sirul dat NU reprezinta un numar.");
		}
	}
	
	private void Ex3()
	{
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("String: ");
		String string = scanner.nextLine();
		System.out.print("Old substring: ");
		String substring1 = scanner.next();
		System.out.print("New substring: ");
		String substring2 = scanner.next();
		
		
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(string);
		string = stringBuilder.reverse().toString();
		System.out.println(string);
		
		string = string.replace(substring1, substring2);
		System.out.println(string);
	}
	
	private void Ex6()
	{
		ArrayList<String> prefixList = new ArrayList<String>();
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Word: ");
		String word = scanner.next();
		
		for (int i = 1; i < word.length(); i++)
		{
			prefixList.add(word.substring(0, i));
		}
		
		for (String prefix: prefixList)
		{
			System.out.println(prefix);
		}
	}
	
	private void Ex8()
	{
		// Declare
		String word;
		StringBuilder guessedWord = new StringBuilder();
		int chances = 6;
		
		// Read
		Scanner scanner = new Scanner(System.in);
		System.out.print("Word: ");
		word = scanner.next();
		
		// Create the word seen by the user
		for (int i = 0; i < word.length(); i++)
		{
			guessedWord.append("_ ");
		}
		System.out.println(guessedWord);
		
		// Play
		while (chances > 0)
		{
			System.out.print("Guess character: ");
			char guessedChar = scanner.next().charAt(0);
			
			int index = word.indexOf(guessedChar);
			
			if (index < 0)
			{
				--chances;
				System.out.println("Wrong. You have " + chances + " more chances.");
			}
			
			while (index >= 0)
			{
				guessedWord.replace(2 * index, 2 * index + 1, String.valueOf(guessedChar));
				index = word.indexOf(guessedChar, index + 1);
			}
			
			System.out.println(guessedWord);
			System.out.println();
			
			
			if (guessedWord.indexOf("_") < 0)
			{
				System.out.println("Congratulations!");
				break;
			}
		}
	}
	
	public static void main(String[] args)
	{
		Main main = new Main();
//		main.Ex1();
//		main.Ex2();
//		main.Ex3();
//		main.Ex6();
		main.Ex8();
	}
}
